<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_customer extends CI_Model
{
    public function get()
    {
        return $this->db->get('customer')->result();
    }

    public function get_where($id)
    {
        $this->db->from('customer');
        $this->db->where('id_customer', $id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return null;
        }
    }

    public function add($data)
    {
        return $this->db->insert("customer", $data);
    }

    public function update($id, $data)
    {
        return $this->db->update('customer', $data, array('id_customer' => $id));
    }

    public function delete($id)
    {
        return $this->db->delete('customer', array('id_customer' => $id));
    }
}

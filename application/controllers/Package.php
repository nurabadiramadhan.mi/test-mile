<?php
// import library dari REST_Controller
require APPPATH . 'libraries/REST_Controller.php';

// extends class dari REST_Controller
class Package extends REST_Controller
{

    // constructor
    public function __construct($config = 'rest')
    {
        parent::__construct($config);

        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method, Authorization");
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE, PATCH");
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method == "OPTIONS") {
            die();
        }
    }


    public function packageGet_get()
    {
        $data = file_get_contents('package.json');
        $json_arr = json_decode($data, true);
        if ($json_arr) {
            $response['status']['code'] = 200;
            $response['status']['response'] = 'success';
            $response['status']['message'] = 'Success get list package';
            $response['result']['data'] = $json_arr;
        } else {
            $response['status']['code'] = 500;
            $response['status']['response'] = 'failed';
            $response['status']['message'] = 'Failed get list package';
            $response['result']['data'] = array();
        }
        $this->response($response);
    }

    public function packageGetById_get()
    {
        if (!$this->get('id')) {
            $response['status']['code'] = 404;
            $response['status']['response'] = 'not found';
            $response['status']['message'] = 'Not found get list package by transaction_id';
            $response['result']['data'] = array();
        } else {
            $data = file_get_contents('package.json');
            $json_arr = json_decode($data, true);

            if ($json_arr) {
                foreach ($json_arr as $index => $json) {
                    if ($json['transaction_id'] == $this->get('id')) {
                        $response['status']['code'] = 200;
                        $response['status']['response'] = 'success';
                        $response['status']['message'] = 'Success get list package by transaction_id';
                        $response['result']['data'] = array($json);
                    } else {
                        $response['status']['code'] = 404;
                        $response['status']['response'] = 'not found';
                        $response['status']['message'] = 'Not found get list package by transaction_id';
                        $response['result']['data'] = array();
                    }
                }
            } else {
                $response['status']['code'] = 500;
                $response['status']['response'] = 'failed';
                $response['status']['message'] = 'Failed get list package by transaction_id';
                $response['result']['data'] = array();
            }
        }
        $this->response($response);
    }

    public function packageAdd_post()
    {
        $data = file_get_contents('package.json');
        $json_arr = json_decode($data, true);

        $currentLocation = array(
            'name' => $this->post('currentLocation')['name'],
            'code' => $this->post('currentLocation')['code'],
            'type' => $this->post('currentLocation')['type']
        );

        $customer_attribute = array(
            'Nama_Sales' => $this->post('customer_attribute')['Nama_Sales'],
            'TOP' => $this->post('customer_attribute')['TOP'],
            'Jenis_Pelanggan' => $this->post('customer_attribute')['Jenis_Pelanggan']
        );

        $connote = array(
            'connote_id' => $this->post('connote_id'),
            'connote_number' => $this->post('connote')['connote_number'],
            'connote_service' => $this->post('connote')['connote_service'],
            'connote_service_price' => $this->post('connote')['connote_service_price'],
            'connote_amount' => $this->post('connote')['connote_amount'],
            'connote_code' => $this->post('connote')['connote_code'],
            'connote_booking_code' => $this->post('connote')['connote_booking_code'],
            'connote_order' => $this->post('connote')['connote_order'],
            'connote_state' => $this->post('connote')['connote_state'],
            'connote_state_id' => $this->post('connote')['connote_state_id'],
            'zone_code_from' => $this->post('connote')['zone_code_from'],
            'zone_code_to' => $this->post('connote')['zone_code_to'],
            'surcharge_amount' => $this->post('connote')['surcharge_amount'],
            'transaction_id' => $this->post('transaction_id'),
            'actual_weight' => $this->post('connote')['actual_weight'],
            'volume_weight' => $this->post('connote')['volume_weight'],
            'chargeable_weight' => $this->post('connote')['chargeable_weight'],
            'created_at' => $this->post('connote')['created_at'],
            'updated_at' => $this->post('connote')['updated_at'],
            'organization_id' => $this->post('organization_id'),
            'location_id' => $this->post('location_id'),
            'connote_total_package' => $this->post('connote')['connote_total_package'],
            'connote_surcharge_amount' => $this->post('connote')['connote_surcharge_amount'],
            'connote_sla_day' => $this->post('connote')['connote_sla_day'],
            'location_name' => $this->post('connote')['location_name'],
            'location_type' => $this->post('connote')['location_type'],
            'source_tariff_db' => $this->post('connote')['source_tariff_db'],
            'id_source_tariff' => $this->post('connote')['id_source_tariff'],
            'pod' => $this->post('connote')['pod'],
            'history' => array()
        );

        $origin_data = array(
            'customer_name' => $this->post('origin_data')['customer_name'],
            'customer_address' => $this->post('origin_data')['customer_address'],
            'customer_email' => $this->post('origin_data')['customer_email'],
            'customer_phone' => $this->post('origin_data')['customer_phone'],
            'customer_address_detail' => $this->post('origin_data')['customer_address_detail'],
            'customer_zip_code' => $this->post('origin_data')['customer_zip_code'],
            'zone_code' => $this->post('origin_data')['zone_code'],
            'organization_id' => $this->post('origin_data')['organization_id'],
            'location_id' => $this->post('location_id')
        );

        $destination_data = array(
            'customer_name' => $this->post('destination_data')['customer_name'],
            'customer_address' => $this->post('destination_data')['customer_address'],
            'customer_email' => $this->post('destination_data')['customer_email'],
            'customer_phone' => $this->post('destination_data')['customer_phone'],
            'customer_address_detail' => $this->post('destination_data')['customer_address_detail'],
            'customer_zip_code' => $this->post('destination_data')['customer_zip_code'],
            'zone_code' => $this->post('destination_data')['zone_code'],
            'organization_id' => $this->post('destination_data')['organization_id'],
            'location_id' => $this->post('destination_data')
        );

        $tampung_data = array();
        $data_koli = $this->post('koli_data');
        foreach ($data_koli as $value) {
            $koli_data = array(
                'koli_length' => $value['koli_length'],
                'awb_url' => $value['awb_url'],
                'created_at' => $value['created_at'],
                'koli_chargeable_weight' => $value['koli_chargeable_weight'],
                'koli_width' => $value['koli_width'],
                'koli_surcharge' => array(),
                'koli_height' => $value['koli_height'],
                'updated_at' => $value['updated_at'],
                'koli_description' => $value['koli_description'],
                'koli_formula_id' => $value['koli_formula_id'],
                'connote_id' => $this->post('connote_id'),
                'koli_volume' => $value['koli_volume'],
                'koli_weight' => $value['koli_weight'],
                'koli_id' => $value['koli_id'],
                'koli_custom_field' => array(
                    'awb_sicepat' => $value['koli_custom_field']['awb_sicepat'],
                    'harga_barang' => $value['koli_custom_field']['harga_barang']
                ),
                'koli_code' => $value['koli_code'],
            );
            array_push($tampung_data, $koli_data);
        }


        $json_arr[] = array(
            'transaction_id' => $this->post('transaction_id'),
            'customer_name' => $this->post('customer_name'),
            'customer_code' => $this->post('customer_code'),
            'transaction_amount' => $this->post('transaction_amount'),
            'transaction_discount' => $this->post('transaction_discount'),
            'transaction_additional_field' => $this->post('transaction_additional_field'),
            'transaction_payment_type' => $this->post('transaction_payment_type'),
            'transaction_state' => $this->post('transaction_state'),
            'transaction_code' => $this->post('transaction_code'),
            'transaction_order' => $this->post('transaction_order'),
            'location_id' => $this->post('location_id'),
            'organization_id' => $this->post('organization_id'),
            'created_at' => $this->post('created_at'),
            'updated_at' => $this->post('updated_at'),
            'transaction_payment_type_name' => $this->post('transaction_payment_type_name'),
            'transaction_cash_amount' => $this->post('transaction_cash_amount'),
            'transaction_cash_change' => $this->post('transaction_cash_change'),
            'customer_attribute' => $customer_attribute,
            'connote' => $connote,
            'connote_id' => $this->post('connote_id'),
            'origin_data' => $origin_data,
            'destination_data' => $destination_data,
            'koli_data' => $tampung_data,
            'custom_field' => $this->post('custom_field'),
            'currentLocation' => $currentLocation
        );

        if ($json_arr) {
            $response['status']['code'] = 200;
            $response['status']['response'] = 'success';
            $response['status']['message'] = 'Success save new data package';
            file_put_contents('package.json', json_encode($json_arr));
        } else {
            $response['status']['code'] = 500;
            $response['status']['response'] = 'failed';
            $response['status']['message'] = 'Failed save new data package';
        }
        $response['result']['data'] = array();
        $this->response($response);
    }

    public function packageUpdate_put()
    {
        $data = file_get_contents('package.json');
        $json_arr = json_decode($data, true);
        $id = $this->put("transaction_id");
        if (!$id) {
            $response['status']['code'] = 404;
            $response['status']['response'] = 'not found';
            $response['status']['message'] = 'Id not found, cannot update data package';
        } else {
            if ($json_arr) {
                foreach ($json_arr as $key => $value) {
                    if ($value['transaction_id'] == $id) {
                        //utama
                        $json_arr[$key]['customer_name'] = $this->put('customer_name');
                        $json_arr[$key]['customer_code'] = $this->put('customer_code');
                        $json_arr[$key]['transaction_amount'] = $this->put('transaction_amount');
                        $json_arr[$key]['transaction_discount'] = $this->put('transaction_discount');
                        $json_arr[$key]['transaction_additional_field'] = $this->put('transaction_additional_field');
                        $json_arr[$key]['transaction_payment_type'] = $this->put('transaction_payment_type');
                        $json_arr[$key]['transaction_state'] = $this->put('transaction_state');
                        $json_arr[$key]['transaction_code'] = $this->put('transaction_code');
                        $json_arr[$key]['transaction_order'] = $this->put('transaction_order');
                        $json_arr[$key]['location_id'] = $this->put('location_id');
                        $json_arr[$key]['organization_id'] = $this->put('organization_id');
                        $json_arr[$key]['created_at'] = $this->put('created_at');
                        $json_arr[$key]['updated_at'] = $this->put('updated_at');
                        $json_arr[$key]['transaction_payment_type_name'] = $this->put('transaction_payment_type_name');
                        $json_arr[$key]['transaction_cash_amount'] = $this->put('transaction_cash_amount');
                        $json_arr[$key]['transaction_cash_change'] = $this->put('transaction_cash_change');

                        //customer attribute
                        $json_arr[$key]['customer_attribute']['Nama_Sales'] = $this->put('customer_attribute')['Nama_Sales'];
                        $json_arr[$key]['customer_attribute']['TOP'] = $this->put('customer_attribute')['TOP'];
                        $json_arr[$key]['customer_attribute']['Jenis_Pelanggan'] = $this->put('customer_attribute')['Jenis_Pelanggan'];

                        //connote
                        $json_arr[$key]['connote']['connote_id'] = $this->put('connote')['connote_id'];
                        $json_arr[$key]['connote']['connote_number'] = $this->put('connote')['connote_number'];
                        $json_arr[$key]['connote']['connote_service'] = $this->put('connote')['connote_service'];
                        $json_arr[$key]['connote']['connote_service_price'] = $this->put('connote')['connote_service_price'];
                        $json_arr[$key]['connote']['connote_amount'] = $this->put('connote')['connote_amount'];
                        $json_arr[$key]['connote']['connote_code'] = $this->put('connote')['connote_code'];
                        $json_arr[$key]['connote']['connote_booking_code'] = $this->put('connote')['connote_booking_code'];
                        $json_arr[$key]['connote']['connote_order'] = $this->put('connote')['connote_order'];
                        $json_arr[$key]['connote']['connote_state'] = $this->put('connote')['connote_state'];
                        $json_arr[$key]['connote']['connote_state_id'] = $this->put('connote')['connote_state_id'];
                        $json_arr[$key]['connote']['zone_code_from'] = $this->put('connote')['zone_code_from'];
                        $json_arr[$key]['connote']['zone_code_to'] = $this->put('connote')['zone_code_to'];
                        $json_arr[$key]['connote']['surcharge_amount'] = $this->put('connote')['surcharge_amount'];
                        $json_arr[$key]['connote']['transaction_id'] = $this->put('transaction_id');
                        $json_arr[$key]['connote']['actual_weight'] = $this->put('connote')['actual_weight'];
                        $json_arr[$key]['connote']['volume_weight'] = $this->put('connote')['volume_weight'];
                        $json_arr[$key]['connote']['chargeable_weight'] = $this->put('connote')['chargeable_weight'];
                        $json_arr[$key]['connote']['created_at'] = $this->put('connote')['created_at'];
                        $json_arr[$key]['connote']['updated_at'] = $this->put('connote')['updated_at'];
                        $json_arr[$key]['connote']['organization_id'] = $this->put('organization_id');
                        $json_arr[$key]['connote']['location_id'] = $this->put('location_id');
                        $json_arr[$key]['connote']['connote_total_package'] = $this->put('connote')['connote_total_package'];
                        $json_arr[$key]['connote']['connote_surcharge_amount'] = $this->put('connote')['connote_surcharge_amount'];
                        $json_arr[$key]['connote']['connote_sla_day'] = $this->put('connote')['connote_sla_day'];
                        $json_arr[$key]['connote']['location_name'] = $this->put('connote')['location_name'];
                        $json_arr[$key]['connote']['location_type'] = $this->put('connote')['location_type'];
                        $json_arr[$key]['connote']['source_tariff_db'] = $this->put('connote')['source_tariff_db'];
                        $json_arr[$key]['connote']['id_source_tariff'] = $this->put('connote')['id_source_tariff'];
                        $json_arr[$key]['connote']['pod'] = $this->put('connote')['pod'];
                        $json_arr[$key]['connote']['history'] = array();

                        $json_arr[$key]['connote_id'] = $this->put('connote_id');

                        //origin data
                        $json_arr[$key]['origin_data']['customer_name'] = $this->put('origin_data')['customer_name'];
                        $json_arr[$key]['origin_data']['customer_address'] = $this->put('origin_data')['customer_address'];
                        $json_arr[$key]['origin_data']['customer_email'] = $this->put('origin_data')['customer_email'];
                        $json_arr[$key]['origin_data']['customer_phone'] = $this->put('origin_data')['customer_phone'];
                        $json_arr[$key]['origin_data']['customer_address_detail'] = $this->put('origin_data')['customer_address_detail'];
                        $json_arr[$key]['origin_data']['customer_zip_code'] = $this->put('origin_data')['customer_zip_code'];
                        $json_arr[$key]['origin_data']['zone_code'] = $this->put('origin_data')['zone_code'];
                        $json_arr[$key]['origin_data']['organization_id'] = $this->put('origin_data')['organization_id'];
                        $json_arr[$key]['origin_data']['location_id'] = $this->put('location_id');

                        //destination data
                        $json_arr[$key]['destination_data']['customer_name'] = $this->put('destination_data')['customer_name'];
                        $json_arr[$key]['destination_data']['customer_address'] = $this->put('destination_data')['customer_address'];
                        $json_arr[$key]['destination_data']['customer_email'] = $this->put('destination_data')['customer_email'];
                        $json_arr[$key]['destination_data']['customer_phone'] = $this->put('destination_data')['customer_phone'];
                        $json_arr[$key]['destination_data']['customer_address_detail'] = $this->put('destination_data')['customer_address_detail'];
                        $json_arr[$key]['destination_data']['customer_zip_code'] = $this->put('destination_data')['customer_zip_code'];
                        $json_arr[$key]['destination_data']['zone_code'] = $this->put('destination_data')['zone_code'];
                        $json_arr[$key]['destination_data']['organization_id'] = $this->put('destination_data')['organization_id'];
                        $json_arr[$key]['destination_data']['location_id'] = $this->put('location_id');

                        $json_arr[$key]['custom_field'] = $this->put('custom_field');

                        //current location
                        $json_arr[$key]['currentLocation']['name'] = $this->put('currentLocation')['name'];
                        $json_arr[$key]['currentLocation']['code'] = $this->put('currentLocation')['code'];
                        $json_arr[$key]['currentLocation']['type'] = $this->put('currentLocation')['type'];

                        // echo $json_arr[$key]['koli_data'][0]['koli_custom_field']['awb_sicepat'];
                        // echo $this->put('koli_data')[0]['koli_custom_field']['awb_sicepat'];
                        $i = 0;
                        foreach ($json_arr[$key]['koli_data'] as $value) {
                            $value['koli_length'] = $this->put('koli_data')[$i]['koli_length'];
                            $value['awb_url'] = $this->put('koli_data')[$i]['awb_url'];
                            $value['created_at'] = $this->put('koli_data')[$i]['created_at'];
                            $value['koli_chargeable_weight'] = $this->put('koli_data')[$i]['koli_chargeable_weight'];
                            $value['koli_width'] = $this->put('koli_data')[$i]['koli_width'];
                            $value['koli_surcharge'] = array();
                            $value['koli_height'] = $this->put('koli_data')[$i]['koli_height'];
                            $value['updated_at'] = $this->put('koli_data')[$i]['updated_at'];
                            $value['koli_description'] = $this->put('koli_data')[$i]['koli_description'];
                            $value['koli_formula_id'] = $this->put('koli_data')[$i]['koli_formula_id'];
                            $value['connote_id'] = $this->put('connote_id');
                            $value['koli_volume'] = $this->put('koli_data')[$i]['koli_volume'];
                            $value['koli_weight'] = $this->put('koli_data')[$i]['koli_weight'];
                            $value['koli_id'] = $this->put('koli_data')[$i]['koli_id'];
                            $value['koli_formula_id'] = $this->put('koli_data')[$i]['koli_formula_id'];
                            $value['koli_custom_field']['awb_sicepat'] = $this->put('koli_data')[$i]['koli_custom_field']['awb_sicepat'];
                            $value['koli_custom_field']['harga_barang'] = $this->put('koli_data')[$i]['koli_custom_field']['harga_barang'];
                            $value['koli_code'] = $this->put('koli_data')[$i]['koli_code'];
                            // echo $value['koli_code']."|".$this->put('koli_data')[$i]['koli_code']."--";
                            $i++;
                        }
                        file_put_contents('package.json', json_encode($json_arr));
                        $response['status']['code'] = 200;
                        $response['status']['response'] = 'success';
                        $response['status']['message'] = 'Success update data package by id ' . $id;
                    } else {
                        $response['status']['code'] = 500;
                        $response['status']['response'] = 'failed';
                        $response['status']['message'] = 'Failed update data package by id ' . $id;
                    }
                }
            }
        }

        $response['result']['data'] = array();
        $this->response($response);
    }

    public function packageUpdatePatch_patch()
    {
        $data = file_get_contents('package.json');
        $json_arr = json_decode($data, true);
        $id = $this->patch("transaction_id");
        if (!$id) {
            $response['status']['code'] = 404;
            $response['status']['response'] = 'not found';
            $response['status']['message'] = 'Id not found, cannot update data package';
        } else {
            if ($json_arr) {
                foreach ($json_arr as $key => $value) {
                    if ($value['transaction_id'] == $id) {
                        //utama
                        $json_arr[$key]['customer_name'] = $this->patch('customer_name');
                        $json_arr[$key]['customer_code'] = $this->patch('customer_code');
                        $json_arr[$key]['transaction_amount'] = $this->patch('transaction_amount');
                        $json_arr[$key]['transaction_discount'] = $this->patch('transaction_discount');
                        $json_arr[$key]['transaction_additional_field'] = $this->patch('transaction_additional_field');
                        $json_arr[$key]['transaction_payment_type'] = $this->patch('transaction_payment_type');
                        $json_arr[$key]['transaction_state'] = $this->patch('transaction_state');
                        $json_arr[$key]['transaction_code'] = $this->patch('transaction_code');
                        $json_arr[$key]['transaction_order'] = $this->patch('transaction_order');
                        $json_arr[$key]['location_id'] = $this->patch('location_id');
                        $json_arr[$key]['organization_id'] = $this->patch('organization_id');
                        $json_arr[$key]['created_at'] = $this->patch('created_at');
                        $json_arr[$key]['updated_at'] = $this->patch('updated_at');
                        $json_arr[$key]['transaction_payment_type_name'] = $this->patch('transaction_payment_type_name');
                        $json_arr[$key]['transaction_cash_amount'] = $this->patch('transaction_cash_amount');
                        $json_arr[$key]['transaction_cash_change'] = $this->patch('transaction_cash_change');

                        //customer attribute
                        $json_arr[$key]['customer_attribute']['Nama_Sales'] = $this->patch('customer_attribute')['Nama_Sales'];
                        $json_arr[$key]['customer_attribute']['TOP'] = $this->patch('customer_attribute')['TOP'];
                        $json_arr[$key]['customer_attribute']['Jenis_Pelanggan'] = $this->patch('customer_attribute')['Jenis_Pelanggan'];

                        //connote
                        $json_arr[$key]['connote']['connote_id'] = $this->patch('connote')['connote_id'];
                        $json_arr[$key]['connote']['connote_number'] = $this->patch('connote')['connote_number'];
                        $json_arr[$key]['connote']['connote_service'] = $this->patch('connote')['connote_service'];
                        $json_arr[$key]['connote']['connote_service_price'] = $this->patch('connote')['connote_service_price'];
                        $json_arr[$key]['connote']['connote_amount'] = $this->patch('connote')['connote_amount'];
                        $json_arr[$key]['connote']['connote_code'] = $this->patch('connote')['connote_code'];
                        $json_arr[$key]['connote']['connote_booking_code'] = $this->patch('connote')['connote_booking_code'];
                        $json_arr[$key]['connote']['connote_order'] = $this->patch('connote')['connote_order'];
                        $json_arr[$key]['connote']['connote_state'] = $this->patch('connote')['connote_state'];
                        $json_arr[$key]['connote']['connote_state_id'] = $this->patch('connote')['connote_state_id'];
                        $json_arr[$key]['connote']['zone_code_from'] = $this->patch('connote')['zone_code_from'];
                        $json_arr[$key]['connote']['zone_code_to'] = $this->patch('connote')['zone_code_to'];
                        $json_arr[$key]['connote']['surcharge_amount'] = $this->patch('connote')['surcharge_amount'];
                        $json_arr[$key]['connote']['transaction_id'] = $this->patch('transaction_id');
                        $json_arr[$key]['connote']['actual_weight'] = $this->patch('connote')['actual_weight'];
                        $json_arr[$key]['connote']['volume_weight'] = $this->patch('connote')['volume_weight'];
                        $json_arr[$key]['connote']['chargeable_weight'] = $this->patch('connote')['chargeable_weight'];
                        $json_arr[$key]['connote']['created_at'] = $this->patch('connote')['created_at'];
                        $json_arr[$key]['connote']['updated_at'] = $this->patch('connote')['updated_at'];
                        $json_arr[$key]['connote']['organization_id'] = $this->patch('organization_id');
                        $json_arr[$key]['connote']['location_id'] = $this->patch('location_id');
                        $json_arr[$key]['connote']['connote_total_package'] = $this->patch('connote')['connote_total_package'];
                        $json_arr[$key]['connote']['connote_surcharge_amount'] = $this->patch('connote')['connote_surcharge_amount'];
                        $json_arr[$key]['connote']['connote_sla_day'] = $this->patch('connote')['connote_sla_day'];
                        $json_arr[$key]['connote']['location_name'] = $this->patch('connote')['location_name'];
                        $json_arr[$key]['connote']['location_type'] = $this->patch('connote')['location_type'];
                        $json_arr[$key]['connote']['source_tariff_db'] = $this->patch('connote')['source_tariff_db'];
                        $json_arr[$key]['connote']['id_source_tariff'] = $this->patch('connote')['id_source_tariff'];
                        $json_arr[$key]['connote']['pod'] = $this->patch('connote')['pod'];
                        $json_arr[$key]['connote']['history'] = array();

                        $json_arr[$key]['connote_id'] = $this->put('connote_id');

                        //origin data
                        $json_arr[$key]['origin_data']['customer_name'] = $this->patch('origin_data')['customer_name'];
                        $json_arr[$key]['origin_data']['customer_address'] = $this->patch('origin_data')['customer_address'];
                        $json_arr[$key]['origin_data']['customer_email'] = $this->patch('origin_data')['customer_email'];
                        $json_arr[$key]['origin_data']['customer_phone'] = $this->patch('origin_data')['customer_phone'];
                        $json_arr[$key]['origin_data']['customer_address_detail'] = $this->patch('origin_data')['customer_address_detail'];
                        $json_arr[$key]['origin_data']['customer_zip_code'] = $this->patch('origin_data')['customer_zip_code'];
                        $json_arr[$key]['origin_data']['zone_code'] = $this->patch('origin_data')['zone_code'];
                        $json_arr[$key]['origin_data']['organization_id'] = $this->patch('origin_data')['organization_id'];
                        $json_arr[$key]['origin_data']['location_id'] = $this->patch('location_id');

                        //destination data
                        $json_arr[$key]['destination_data']['customer_name'] = $this->patch('destination_data')['customer_name'];
                        $json_arr[$key]['destination_data']['customer_address'] = $this->patch('destination_data')['customer_address'];
                        $json_arr[$key]['destination_data']['customer_email'] = $this->patch('destination_data')['customer_email'];
                        $json_arr[$key]['destination_data']['customer_phone'] = $this->patch('destination_data')['customer_phone'];
                        $json_arr[$key]['destination_data']['customer_address_detail'] = $this->patch('destination_data')['customer_address_detail'];
                        $json_arr[$key]['destination_data']['customer_zip_code'] = $this->patch('destination_data')['customer_zip_code'];
                        $json_arr[$key]['destination_data']['zone_code'] = $this->patch('destination_data')['zone_code'];
                        $json_arr[$key]['destination_data']['organization_id'] = $this->patch('destination_data')['organization_id'];
                        $json_arr[$key]['destination_data']['location_id'] = $this->patch('location_id');

                        $json_arr[$key]['custom_field'] = $this->patch('custom_field');

                        //current location
                        $json_arr[$key]['currentLocation']['name'] = $this->patch('currentLocation')['name'];
                        $json_arr[$key]['currentLocation']['code'] = $this->patch('currentLocation')['code'];
                        $json_arr[$key]['currentLocation']['type'] = $this->patch('currentLocation')['type'];

                        // echo $json_arr[$key]['koli_data'][0]['koli_custom_field']['awb_sicepat'];
                        // echo $this->put('koli_data')[0]['koli_custom_field']['awb_sicepat'];
                        $i = 0;
                        foreach ($json_arr[$key]['koli_data'] as $value) {
                            $value['koli_length'] = $this->patch('koli_data')[$i]['koli_length'];
                            $value['awb_url'] = $this->patch('koli_data')[$i]['awb_url'];
                            $value['created_at'] = $this->patch('koli_data')[$i]['created_at'];
                            $value['koli_chargeable_weight'] = $this->patch('koli_data')[$i]['koli_chargeable_weight'];
                            $value['koli_width'] = $this->patch('koli_data')[$i]['koli_width'];
                            $value['koli_surcharge'] = array();
                            $value['koli_height'] = $this->patch('koli_data')[$i]['koli_height'];
                            $value['updated_at'] = $this->patch('koli_data')[$i]['updated_at'];
                            $value['koli_description'] = $this->patch('koli_data')[$i]['koli_description'];
                            $value['koli_formula_id'] = $this->patch('koli_data')[$i]['koli_formula_id'];
                            $value['connote_id'] = $this->patch('connote_id');
                            $value['koli_volume'] = $this->patch('koli_data')[$i]['koli_volume'];
                            $value['koli_weight'] = $this->patch('koli_data')[$i]['koli_weight'];
                            $value['koli_id'] = $this->patch('koli_data')[$i]['koli_id'];
                            $value['koli_formula_id'] = $this->patch('koli_data')[$i]['koli_formula_id'];
                            $value['koli_custom_field']['awb_sicepat'] = $this->patch('koli_data')[$i]['koli_custom_field']['awb_sicepat'];
                            $value['koli_custom_field']['harga_barang'] = $this->patch('koli_data')[$i]['koli_custom_field']['harga_barang'];
                            $value['koli_code'] = $this->patch('koli_data')[$i]['koli_code'];
                            // echo $value['koli_code']."|".$this->put('koli_data')[$i]['koli_code']."--";
                            $i++;
                        }
                        file_put_contents('package.json', json_encode($json_arr));
                        $response['status']['code'] = 200;
                        $response['status']['response'] = 'success';
                        $response['status']['message'] = 'Success update data package by id ' . $id;
                    } else {
                        $response['status']['code'] = 500;
                        $response['status']['response'] = 'failed';
                        $response['status']['message'] = 'Failed update data package by id ' . $id;
                    }
                }
            }
        }

        $response['result']['data'] = array();
        $this->response($response);
    }

    public function packageDelete_delete()
    {

        $id = $this->delete("id");
        if (!$id) {
            $response['status']['code'] = 404;
            $response['status']['response'] = 'not found';
            $response['status']['message'] = 'Id not found, cannot delete data package';
        } else {
            $data = file_get_contents('package.json');
            $json_arr = json_decode($data, true);

            if ($json_arr) {
                foreach ($json_arr as $index => $json) {
                    if ($json['transaction_id'] == $id) {
                        unset($json_arr[$index]);
                        $response['status']['code'] = 200;
                        $response['status']['response'] = 'success';
                        $response['status']['message'] = 'Success delete data package by id ' . $id;
                        $json_arr = array_values($json_arr);

                        file_put_contents('package.json', json_encode($json_arr));
                    } else {
                        $response['status']['code'] = 404;
                        $response['status']['response'] = 'not found';
                        $response['status']['message'] = 'Id not found, cannot delete data package';
                    }
                }
            } else {
                $response['status']['code'] = 500;
                $response['status']['response'] = 'failed';
                $response['status']['message'] = 'Failed delete data package by id ' . $id;
            }
        }

        $response['result']['data'] = array();
        $this->response($response);
    }
}
